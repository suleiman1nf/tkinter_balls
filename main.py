from tkinter import *
from random import randrange as rnd, choice
import time
import random
root = Tk()
root.geometry('800x600')

canv = Canvas(root,bg='white')
canv.pack(fill=BOTH,expand=1)

colors = ['red','orange','yellow','green','blue']
score = 0

def create_balls(_count):
    global count, speeds, balls, ballsizes
    count = _count
    speeds = [[0] * 2 for i in range(count)]
    balls = [[0] * 1 for i in range(count)]
    ballsizes = [[0] * 1 for i in range(count)]
    for i in range(count):
        new_ball(i)
        speeds[i][0] = random.random() * 10 + 10
        speeds[i][1] = random.random() * 10 + 10

def new_ball(index):
    x = rnd(100,700)
    y = rnd(100,500)
    r = rnd(30,50)
    ballsizes[index] = r
    balls[index] = canv.create_oval(x-r,y-r,x+r,y+r,fill = choice(colors), width=0, tags = "Ball"+str(index))


def update():
    for i in range(count):
        move(i, speeds[i][0], speeds[i][1])
        check_wall_colision(i)
    i = 0
    while (i < count-1):
        j = i + 1
        while (j < count):
            check_ball_collision(i,j)
            j = j + 1
        i = i + 1
    root.after(100,update)

def check_wall_colision(index):
    if((canv.coords(balls[index])[0] + 2*ballsizes[index] >= 800) or (canv.coords(balls[index])[0] <= 0)):
        speeds[index][0] = -speeds[index][0]
    if(canv.coords(balls[index])[1] <= 0 or canv.coords(balls[index])[1] + 2*ballsizes[index] >= 600):
        speeds[index][1] = -speeds[index][1]
        
def check_ball_collision(index1, index2):
    x1 = canv.coords(balls[index1])[0] + ballsizes[index1]
    y1 = canv.coords(balls[index1])[1] + ballsizes[index1]
    r1 = ballsizes[index1]
    x2 = canv.coords(balls[index2])[0] + ballsizes[index2]
    y2 = canv.coords(balls[index2])[1] + ballsizes[index2]
    r2 = ballsizes[index2]
    dx = x1 - x2
    dy = y1 - y2
    distance = (dx*dx+dy*dy)**(1/2)
    if(distance < r1 + r2):
       speeds[index1][0] = -speeds[index1][0]
       speeds[index1][1] = -speeds[index1][1]
       speeds[index2][0] = -speeds[index2][0]
       speeds[index2][1] = -speeds[index2][1]


def move(index, speedX, speedY):
    canv.move(balls[index], speedX, speedY)

def click(event):
    global score
    for i in range(count):
        x = canv.coords(balls[i])[0] + ballsizes[i]
        y = canv.coords(balls[i])[1] + ballsizes[i]
        r = ballsizes[i]
        if(is_mouse_touched_ball(x,y,r,event.x, event.y)):
            score += 1
            canv.itemconfig(score_text, text = "score = " + str(score))
    return

def is_mouse_touched_ball(ball_x,ball_y,ball_r,mouse_x,mouse_y):
    distance = (abs(mouse_x-ball_x)**2 + abs(mouse_y-ball_y)**2)**(1/2)
    return ball_r>=distance

canv.create_rectangle(0, 0, 800, 600, 
            outline="#fb0", fill="#fb0")
score_text = canv.create_text(10,10, anchor="nw", text="score = 0")
create_balls(3)
update()
canv.bind('<Button-1>', click)
mainloop()

